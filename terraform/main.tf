provider "kubernetes" {
  config_context_cluster   = "minikube"
}

resource "kubernetes_role" "test-rbac" {
  metadata {
    name = "rebac-reto"
    namespace = "default"
    labels = {
      name = "rebac-reto"
    }
  }

  rule {
    api_groups     = [""]
    resources      = ["pods"]
    verbs          = ["get", "list", "watch"]
  }
}

