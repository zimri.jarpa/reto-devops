#-- indicar el uso de node
FROM node:erbium-alpine3.11
#-- se indica la versión
ENV NODE_VERSION 16.0.0
#-- definir la variable de entorno en donde se aloja la app
ENV REPO_HOME=/app
#-- seteamos la ruta de trabajo
WORKDIR ${REPO_HOME}
#-- copiamos los archivos de node
COPY index.js .
COPY package.json .
# -- instalamos la npm
RUN npm install 

#-- Ejecutar la app con otro usuario
RUN addgroup -S nodeapp                          && \
    adduser -s /bin/bash -S -G nodeapp nodeapp     

USER nodeapp

EXPOSE 3000

CMD [ "node", "index.js"]