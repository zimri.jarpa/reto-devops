all: bajar_docker builder_docker deploy prueba_app_k8s terraform
bajar_docker: 
			docker-compose down 

builder_docker:
			docker-compose up -d --build --force-recreate

deploy:
			kubectl apply -f kubernetes

prueba_app_k8s:
			kubectl expose deployment node-app --type=NodePort --port=80
terraform:
			terraform init && terraform plan && terraform apply --auto-approve	